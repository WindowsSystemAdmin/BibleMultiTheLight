package org.hlwd.bible;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;

public class BibleWidgetVerseProvider extends AppWidgetProvider
{
    private SCommon _s = null;
    private int     WIDGET_LAYOUT_ID = R.layout.bible_widget_verse_light;
    private boolean IS_WIDGET_LAYOUT_DARK = false;

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds)
    {
        try
        {
            if (PCommon._isDebugVersion) System.out.println("onUpdate => count: " + appWidgetIds.length);

            for (int appWidgetId : appWidgetIds)
            {
                final WidgetVerseBO widgetVerse = CommonWidget.GetWidgetRandomVerse(context);
                CommonWidgetVerse.UpdateAppWidget(context, appWidgetId, widgetVerse);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }
}
