3.18 - 20190825


== English ==

• Support for Android 10.0 (Q)
• Notification to stop the Audio Bible



== Français ==

• Support pour Android 10.0 (Q)
• Notification pour stopper la Bible Audio



== Italiano ==

• Supporto per Android 10.0 (Q)
• Notificazione per fermare la Bibbia Audio



== Español ==

• Soporte para Android 10.0 (Q)
• Notificación para detener la Biblia Audio



== Portugués ==

• Suporte para Android 10.0 (Q)
• Notificação para parar a Bíblia Audio
