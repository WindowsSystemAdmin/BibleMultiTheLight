Bibbia multi lingue, gratuita, offline, senza pubblicità, interamente in inglese, francese, italiano, spagnolo, portoghese.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald.

Facile da usare con funzioni di ricerca veloce e di condivisione, piani de lettura, Bibbia audio, articoli, riferimenti incrociati, widgets.

Funziona anche su Android TV, Chromebook.

