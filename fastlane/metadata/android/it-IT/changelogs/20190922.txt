• Non è necessario riavviare l'applicazione quando si modificano alcune preferenze
• La lingua selezionata nell'applicazione potrebbe essere diversa dalla lingua del sistema
• Selezionando una Bibbia, questo definirà la lingua utilizzata nell'applicazione