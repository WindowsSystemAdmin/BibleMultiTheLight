Bíblia em vários idiomas, gratuita, off-line, sem anúncios, totalmente em inglês, francês, italiano, espanhol, português.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald.

Fácil de usar com funções de pesquisa rápida, compartilhamento, planos de leitura, Bíblia audio, artigos, referências cruzadas, widgets.

Também funciona na Android TV, Chromebook.
